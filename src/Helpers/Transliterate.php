<?php

namespace Rus\RusToLatSlug\Helpers;

class Transliterate
{
    public static function rus2latConvert($string): string
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public static function rusToLat($data, $field = NULL): array | string
    {
        $convert_field = is_null($field) ? config('rustolat.convert_field') : $field;
        $convert = is_array($data) ? $data[$convert_field] : $data;

        $value = Transliterate::rus2latConvert($convert);
        $value = strtolower($value);
        $value = trim($value);

        if (!is_null(config('rustolat.convert_max_words'))) {
            $value = explode(' ', $value);
            $value = config('rustolat.convert_field')
                ? implode(' ', array_slice($value, 0, config('rustolat.convert_max_words')))
                : $value;
        }

        $value = preg_replace('~[^-a-z0-9_]+~u', config('rustolat.convert_delimiter'), $value);

        if (is_array($data)){
            $data[$convert_field] = trim($value, config('rustolat.convert_delimiter'));
            return $data;
        }

        return trim($value, config('rustolat.convert_delimiter'));

    }
}
