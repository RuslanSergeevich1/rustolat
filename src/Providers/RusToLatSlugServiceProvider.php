<?php

namespace Rus\RusToLatSlug\Providers;

use Illuminate\Support\ServiceProvider;

class RusToLatSlugServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
        require_once __DIR__ . '/../Helpers/Transliterate.php';

        $this->publishes([
            __DIR__ . '/../config/rustolat.php' => config_path('rustolat.php'),
        ], 'rustolat-config');
    }
}
