<?php

return [
    'convert_field' => 'slug',
    'convert_delimiter' => '-',
    'convert_max_words' => 6,
];
