## Конвертирование URL (Rus to Lat) в Laravel

### Установка
```
composer require rustikjan/rus-to-lat-slug
```
### Публикация конфига
```
php artisan vendor:publish --tag=rustolat-config
```
### Подключение:
```
use Rus\RusToLatSlug\Helpers\Transliterate;
```
### Использование:
```
Transliterate::rusToLat($array, $field)
```
- $array - массив данных

- $field - поле в массиве, которое нужно конвертировать

Для конвертирования отдельной строки можно использовать
```
Transliterate::rusToLat($string)
```
$string - строка, которую нужно конвертировать

### Конфигурация :
```
'convert_field' => 'slug' // (string)
```
convert_field - по какому полю будет конвертация, по умолчанию 'slug'
```
'convert_delimiter' => '-' // (string)
```
convert_delimiter - на что будут заменены пробелы в конвертируемой строке , по умолчанию '-'
```
'convert_max_words' => 5, // (number | null)
```
convert_max_words - максимальное кол-во слов, возврещаемое после конвертации, по умолчанию 5

    
